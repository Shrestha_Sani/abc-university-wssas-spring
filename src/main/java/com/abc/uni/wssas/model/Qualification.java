package com.abc.uni.wssas.model;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Qualification implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="degree_name")
	private String degreeName;
	
	@Column(name="division")
	private String division;
	
	@Column(name="university_name")
	private String universityName;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@ManyToOne
	private User user;
	

	@Override
	public String toString() {
		return "Qualification [id=" + id + ", degreeName=" + degreeName + ", division=" + division + ", universityName="
				+ universityName + "]";
	}
	
	


	

}
