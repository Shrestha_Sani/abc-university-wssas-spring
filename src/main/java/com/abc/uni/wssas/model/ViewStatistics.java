package com.abc.uni.wssas.model;

import java.util.List;

public class ViewStatistics {
	List<Announcement> announcements;
	List<DiscussionForum> discussionForums;
	List<ResourceArticles> resourceArticles;
	
	public List<Announcement> getAnnouncements() {
		return announcements;
	}
	public void setAnnouncements(List<Announcement> announcements) {
		this.announcements = announcements;
	}
	public List<DiscussionForum> getDiscussionForums() {
		return discussionForums;
	}
	public void setDiscussionForums(List<DiscussionForum> discussionForums) {
		this.discussionForums = discussionForums;
	}
	public List<ResourceArticles> getResourceArticles() {
		return resourceArticles;
	}
	public void setResourceArticles(List<ResourceArticles> resourceArticles) {
		this.resourceArticles = resourceArticles;
	}
 }
