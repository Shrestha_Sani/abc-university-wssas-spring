package com.abc.uni.wssas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"discussionForumPosts"})
public class DiscussionForum implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String subject;
	
	@Column
	private String message;
	
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Course course;
	
	@OneToMany(mappedBy = "discussionForum", cascade = CascadeType.ALL)
	private List<DiscussionForumPosts> discussionForumPosts;

	@ManyToOne
	private User updatedBy;
	
	@Column(name ="updated_date")
	@Temporal(TemporalType.DATE)
	private Date updatedDate;
	
	@Transient
	private int totalPosts;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<DiscussionForumPosts> getDiscussionForumPosts() {
		return discussionForumPosts;
	}

	public void setDiscussionForumPosts(List<DiscussionForumPosts> discussionForumPosts) {
		this.discussionForumPosts = discussionForumPosts;
	}

	public int getTotalPosts() {
		return totalPosts;
	}

	public void setTotalPosts(int totalPosts) {
		this.totalPosts = totalPosts;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
