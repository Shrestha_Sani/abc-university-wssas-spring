package com.abc.uni.wssas.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"discussionForumList", "users"})
public class Course implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "course_name")
	private String courseName;
		
	@Column( name = "course_code")
	private String courseCode;
	
	@ManyToMany(mappedBy = "courses")
	private List<User> users;
	
	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
	private List<DiscussionForum> discussionForumList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	
	@Override
	public String toString() {
		return "Course [id=" + id + ", courseName=" + courseName + ", courseCode=" + courseCode + "]";
	}
	
	
}
