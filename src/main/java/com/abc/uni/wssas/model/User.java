package com.abc.uni.wssas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Temporal(TemporalType.DATE)
	private Date dob;

	@OneToOne(cascade = CascadeType.ALL)	
    @JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;
	
	@Column(name = "phone_num")
	private String phoneNum;

	@Column(name="gender")
	private String gender;
	
	@ManyToMany
	private List<Degree> degrees;

	@ManyToMany
	private List<Course> courses;

	@Column(name="email")
	private String email;
	
	@Column(name = "user_name")
	private String username;

	@Column(name ="password")
	private String password;
	
	@Column(name="profile_pic")
	private String profilePic;
	
	@ManyToOne(fetch = FetchType.EAGER)
		private Role role;

	@OneToMany(mappedBy = "user")
	private List<ResourceArticles> resourceArticles;
	
	@OneToMany(mappedBy = "user")
	private List<Announcement> announcement;
	
	
	@OneToMany(mappedBy= "user")
	private List<DiscussionForum> discussionForumList;
	
	@OneToMany(mappedBy= "user")
	private List<DiscussionForumPosts> forumPosts;
	
	@Transient
	private boolean duplicateUser;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Qualification> qualification;
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}



	public String getPhoneNum() {
		return phoneNum;
	}


	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public List<Degree> getDegrees() {
		return degrees;
	}


	public void setDegrees(List<Degree> degrees) {
		this.degrees = degrees;
	}


	public List<Course> getCourses() {
		return courses;
	}


	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUsername() {
		return username;
	}


	public void setUserId(String userId) {
		this.username = userId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getProfilePic() {
		return profilePic;
	}


	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}


	public boolean isDuplicateUser() {
		return duplicateUser;
	}


	public void setDuplicateUser(boolean duplicateUser) {
		this.duplicateUser = duplicateUser;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", dob=" + dob + ", phoneNum="
				+ phoneNum + ", gender=" + gender + ", email=" + email + ", userId=" + username + ", password=" + password
				+ ", profilePic=" + profilePic + "]";
	}

	

}
