package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.CourseDao;
import com.abc.uni.wssas.model.Course;

@RestController
@RequestMapping("/course")
@CrossOrigin(origins = "http://localhost:3000")
public class CoursePresenter {
	@Autowired
	CourseDao courseDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<Course>> findAll(){
		List<Course> courses= courseDao.findAll();
		return new ResponseEntity<List<Course>>(courses, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/count")
	public int countCourse(){
		return courseDao.findAll().size();
	}
	
	@PostMapping("/save")
	public ResponseEntity<Course> save(@RequestBody Course course) {
		Course courseEntity= courseDao.save(course);
		return new ResponseEntity<Course>(courseEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Course> update(@PathVariable(value ="id") Long id, @RequestBody Course course) {
		Course dbCourse= courseDao.findById(id);
		if(dbCourse == null) {
			return ResponseEntity.notFound().build();
		}
		Course courseEntity= courseDao.update(course, id);
		return new ResponseEntity<Course>(courseEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Course> delete(@PathVariable("id") Long id) {
		Course course= courseDao.findById(id);
		if(course == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			courseDao.delete(course);
			return ResponseEntity.ok().build();
		}
	}

}
