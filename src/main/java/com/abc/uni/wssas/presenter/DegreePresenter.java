package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.DegreeDao;
import com.abc.uni.wssas.model.Degree;

@RestController
@RequestMapping("/degree")
@CrossOrigin(origins = "http://localhost:3000")
public class DegreePresenter {
	@Autowired
	DegreeDao degreeDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<Degree>> findAll(){
		List<Degree> degrees= degreeDao.findAll();
		return new ResponseEntity<List<Degree>>(degrees, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<Degree> save(@RequestBody Degree degree) {
		Degree degreeEntity= degreeDao.save(degree);
		return new ResponseEntity<Degree>(degreeEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Degree> update(@PathVariable(value ="id") Long id, @RequestBody Degree degree) {
		Degree dbDegree= degreeDao.findById(id);
		if(dbDegree == null) {
			return ResponseEntity.notFound().build();
		}
		Degree degreeEntity= degreeDao.update(degree, id);
		return new ResponseEntity<Degree>(degreeEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Degree> findById(@PathVariable(value ="id") Long id) {
		Degree degree= degreeDao.findById(id);
		if(degree == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(degree);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Degree> delete(@PathVariable("id") Long id) {
		Degree degree= degreeDao.findById(id);
		if(degree == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			degreeDao.delete(degree);
			return ResponseEntity.ok().build();
		}
	}
}
