package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.AnnouncementDao;
import com.abc.uni.wssas.model.Announcement;
import com.abc.uni.wssas.model.User;

@RestController
@RequestMapping("/announcement")
@CrossOrigin
public class AnnouncementPresenter {
	@Autowired
	AnnouncementDao announcementDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<Announcement>> findAll(){
		List<Announcement> announcements= announcementDao.findAll();
		return new ResponseEntity<List<Announcement>>(announcements, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/new/count")
	public int getNewAnnouncementCount(){
		return announcementDao.getNewAnnouncementCount();
	}
	
	@GetMapping("/notifications")
	public ResponseEntity<List<Announcement>> getNotifications(){
		List<Announcement> announcements= announcementDao.getNotifications();
		return new ResponseEntity<List<Announcement>>(announcements, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<Announcement> save(@RequestBody Announcement announcement) {
		Announcement announcementEntity= announcementDao.save(announcement);
		return new ResponseEntity<Announcement>(announcementEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Announcement> update(@PathVariable(value ="id") Long id, @RequestBody Announcement announcement) {
		Announcement dbAnnouncement= announcementDao.findById(id);
		if(dbAnnouncement == null) {
			return ResponseEntity.notFound().build();
		}
		Announcement announcementEntity= announcementDao.update(announcement, id);
		return new ResponseEntity<Announcement>(announcementEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/notify/{id}")
	public ResponseEntity<Announcement> notify(@PathVariable(value ="id") Long id) {
		Announcement dbAnnouncement= announcementDao.findById(id);
		if(dbAnnouncement == null) {
			return ResponseEntity.notFound().build();
		}
		Announcement announcementEntity= announcementDao.notify(id);
		return new ResponseEntity<Announcement>(announcementEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Announcement> findById(@PathVariable(value ="id") Long id) {
		Announcement announcement= announcementDao.findById(id);
		if(announcement == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(announcement);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> delete(@PathVariable("id") Long id) {
		Announcement announcement= announcementDao.findById(id);
		if(announcement == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			announcementDao.delete(announcement);
			return ResponseEntity.ok().build();
		}
	}
}
