package com.abc.uni.wssas.presenter;

import com.abc.uni.wssas.AbcUserDetailsService;
import com.abc.uni.wssas.JwtTokenUtil;
import com.abc.uni.wssas.LoginRequest;
import com.abc.uni.wssas.LoginSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class LoginPresenter {
    @Autowired
    AbcUserDetailsService userDetailService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequest loginRequest) {
		boolean authenticated = true;
		String message = "";
        try {
            authenticate(loginRequest.getUsername(), loginRequest.getPassword());
        } catch (DisabledException e) {
            System.out.println("DisabledException = " + e);
            message = "User is disabled";
            authenticated = false;
        } catch (BadCredentialsException e) {
            System.out.println("BadCredentialsException = " );
            message = "Invalid Credential";
            authenticated = false;
        } catch (Exception e) {
            System.out.println("e = " + e);
            message = "exception occurred";
            authenticated = false;
        }
        System.out.println("authenticated = " + authenticated);
        if (authenticated==true) {
            UserDetails userDetails = userDetailService.loadUserByUsername(loginRequest.getUsername());
			final String token = jwtTokenUtil.generateToken(userDetails);
			GrantedAuthority role = userDetails.getAuthorities().stream().findFirst().get();
			return ResponseEntity.ok(new LoginSuccess(token, userDetails.getUsername(), role.getAuthority(),authenticated,message));
		}else {
        	return ResponseEntity.ok(new LoginSuccess(null,"","",authenticated,message));
		}
    }

    private void authenticate(String username, String password) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

    }
}
