package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.DiscussionForumPostsDao;
import com.abc.uni.wssas.model.DiscussionForumPosts;
import com.abc.uni.wssas.model.User;

@RestController
@RequestMapping("/discussionForumPost")
@CrossOrigin(origins = "http://localhost:3000")
public class discussionForumPostPresenter {
	@Autowired
	DiscussionForumPostsDao dfPostDao;
	
	@GetMapping("/discussionForum/{id}")
	public ResponseEntity<List<DiscussionForumPosts>> findAll(@PathVariable(value ="id") Long id){
		List<DiscussionForumPosts> discussionForums= dfPostDao.findByDiscussionForum(id);
		return new ResponseEntity<List<DiscussionForumPosts>>(discussionForums, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<DiscussionForumPosts> save(@RequestBody DiscussionForumPosts discussionForumPost) {
		DiscussionForumPosts discussionForumEntity= dfPostDao.save(discussionForumPost);
		return new ResponseEntity<DiscussionForumPosts>(discussionForumEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<DiscussionForumPosts> update(@PathVariable(value ="id") Long id, @RequestBody DiscussionForumPosts discussionForum) {
		DiscussionForumPosts dbDiscussionForumPosts= dfPostDao.findById(id);
		if(dbDiscussionForumPosts == null) {
			return ResponseEntity.notFound().build();
		}
		DiscussionForumPosts discussionForumEntity= dfPostDao.update(discussionForum, id);
		return new ResponseEntity<DiscussionForumPosts>(discussionForumEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DiscussionForumPosts> findById(@PathVariable(value ="id") Long id) {
		DiscussionForumPosts discussionForum= dfPostDao.findById(id);
		if(discussionForum == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(discussionForum);
		}
	}
	
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> delete(@PathVariable("id") Long id) {
		DiscussionForumPosts discussionForum= dfPostDao.findById(id);
		if(discussionForum == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			dfPostDao.delete(discussionForum);
			return ResponseEntity.ok().build();
		}
	}
}
