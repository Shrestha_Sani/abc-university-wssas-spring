package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.RoleDao;
import com.abc.uni.wssas.model.Role;

@RestController
@RequestMapping("/role")
@CrossOrigin(origins = "http://localhost:3000")
public class RolePresenter {
	
	@Autowired
	RoleDao roleDao;
	
	@GetMapping("/data")
	public List<Role> getRoles(){
		return roleDao.getRoles();
	}
	
	
}
