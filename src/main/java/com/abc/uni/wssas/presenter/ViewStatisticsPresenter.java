package com.abc.uni.wssas.presenter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.AnnouncementDao;
import com.abc.uni.wssas.dao.DiscussionForumDao;
import com.abc.uni.wssas.dao.ResourceArticlesDao;
import com.abc.uni.wssas.model.Report;
import com.abc.uni.wssas.model.ViewStatistics;

@RestController
@RequestMapping("/view-statistics")
@CrossOrigin
public class ViewStatisticsPresenter {
	
	@Autowired
	AnnouncementDao announcementDao;
	
	@Autowired
	DiscussionForumDao discussionForumDao;
	
	@Autowired
	ResourceArticlesDao resourceArticlesDao;
	
	@GetMapping("/yesterday")
	public ResponseEntity<ViewStatistics> getStatisticsYesterday(){
		ViewStatistics viewStatistics= new ViewStatistics();
		viewStatistics.setAnnouncements(announcementDao.getYesterdayData());
		viewStatistics.setDiscussionForums(discussionForumDao.getYesterdayData());
		viewStatistics.setResourceArticles(resourceArticlesDao.getYesterdayData());
		return new ResponseEntity<ViewStatistics>(viewStatistics, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/7-days")
	public ResponseEntity<ViewStatistics> getStatisticsLast7Days(){
		ViewStatistics viewStatistics= new ViewStatistics();
		viewStatistics.setAnnouncements(announcementDao.getLastWeekData());
		viewStatistics.setDiscussionForums(discussionForumDao.getLastWeekData());
		viewStatistics.setResourceArticles(resourceArticlesDao.getLastWeekData());
		return new ResponseEntity<ViewStatistics>(viewStatistics, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/15-days")
	public ResponseEntity<ViewStatistics> getStatisticsLast15Days(){
		ViewStatistics viewStatistics= new ViewStatistics();
		viewStatistics.setAnnouncements(announcementDao.getLast15daysData());
		viewStatistics.setDiscussionForums(discussionForumDao.getLast15daysData());
		viewStatistics.setResourceArticles(resourceArticlesDao.getLast15daysData());
		return new ResponseEntity<ViewStatistics>(viewStatistics, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/30-days")
	public ResponseEntity<ViewStatistics> getStatisticsLastMonth(){
		ViewStatistics viewStatistics= new ViewStatistics();
		viewStatistics.setAnnouncements(announcementDao.getLastMonthData());
		viewStatistics.setDiscussionForums(discussionForumDao.getLastMonthData());
		viewStatistics.setResourceArticles(resourceArticlesDao.getLastMonthData());
		return new ResponseEntity<ViewStatistics>(viewStatistics, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/dates")
	public ResponseEntity<ViewStatistics> getStatisticsBetweenDates(@RequestBody Report report){
		ViewStatistics viewStatistics= new ViewStatistics();
		viewStatistics.setAnnouncements(announcementDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate()));
		viewStatistics.setDiscussionForums(discussionForumDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate()));
		viewStatistics.setResourceArticles(resourceArticlesDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate()));
		return new ResponseEntity<ViewStatistics>(viewStatistics, new HttpHeaders(), HttpStatus.OK);
	}
	
	
}
