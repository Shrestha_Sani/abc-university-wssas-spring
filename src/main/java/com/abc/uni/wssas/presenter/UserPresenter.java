package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.UserDao;
import com.abc.uni.wssas.model.ChangePasswordRequest;
import com.abc.uni.wssas.model.ChangePasswordResponse;
import com.abc.uni.wssas.model.User;


@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserPresenter {

	@Autowired
	UserDao userDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<User>> findAll(){
		List<User> users= userDao.findAll();
		return new ResponseEntity<List<User>>(users, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/student/count")
	public int findTotalStudent() {
		return userDao.getStudentCount();
	}
	
	@GetMapping("/staff/count")
	public int findTotalStaff() {
		return userDao.getStaffCount();
	}
	
	@PostMapping("/save")
	public ResponseEntity<User> save(@RequestBody User user) {
		User userEntity= userDao.save(user);
		return new ResponseEntity<User>(userEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/username/{username}")
	public ResponseEntity<User> findByUsername(@PathVariable(value ="username") String username){
		User user= userDao.findByUserName(username);
		return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<User> update(@PathVariable(value ="id") Long id, @RequestBody User user) {
		User dbUser= userDao.findById(id);
		if(dbUser == null) {
			return ResponseEntity.notFound().build();
		}
		User userEntity= userDao.update(user, id);
		return new ResponseEntity<User>(userEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> findById(@PathVariable(value ="id") Long id) {
		User user= userDao.findById(id);
		if(user == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(user);
		}
	}
	
	@PostMapping("/change-password")
	public ResponseEntity<ChangePasswordResponse> changePassword(@RequestBody ChangePasswordRequest request){
		ChangePasswordResponse response= userDao.changePassword(request);
		return new ResponseEntity<ChangePasswordResponse>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> delete(@PathVariable("id") Long id) {
		User user= userDao.findById(id);
		if(user == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			userDao.delete(user);
			return ResponseEntity.ok().build();
		}
	}
}
