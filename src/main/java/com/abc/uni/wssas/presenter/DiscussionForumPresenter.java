package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.DiscussionForumDao;
import com.abc.uni.wssas.model.DiscussionForum;
import com.abc.uni.wssas.model.User;

@RestController
@RequestMapping("/discussionForum")
@CrossOrigin(origins = "http://localhost:3000")
public class DiscussionForumPresenter {
	@Autowired
	DiscussionForumDao discussionForumDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<DiscussionForum>> findAll(){
		List<DiscussionForum> discussionForums= discussionForumDao.findAll();
		return new ResponseEntity<List<DiscussionForum>>(discussionForums, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/count")
	public int totalCount() {
		return discussionForumDao.findAll().size();
	}
	
	@PostMapping("/save")
	public ResponseEntity<DiscussionForum> save(@RequestBody DiscussionForum discussionForum) {
		DiscussionForum discussionForumEntity= discussionForumDao.save(discussionForum);
		return new ResponseEntity<DiscussionForum>(discussionForumEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<DiscussionForum> update(@PathVariable(value ="id") Long id, @RequestBody DiscussionForum discussionForum) {
		DiscussionForum dbDiscussionForum= discussionForumDao.findById(id);
		if(dbDiscussionForum == null) {
			return ResponseEntity.notFound().build();
		}
		DiscussionForum discussionForumEntity= discussionForumDao.update(discussionForum, id);
		return new ResponseEntity<DiscussionForum>(discussionForumEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DiscussionForum> findById(@PathVariable(value ="id") Long id) {
		DiscussionForum discussionForum= discussionForumDao.findById(id);
		if(discussionForum == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(discussionForum);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> delete(@PathVariable("id") Long id) {
		DiscussionForum discussionForum= discussionForumDao.findById(id);
		if(discussionForum == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			discussionForumDao.delete(discussionForum);
			return ResponseEntity.ok().build();
		}
	}
}
