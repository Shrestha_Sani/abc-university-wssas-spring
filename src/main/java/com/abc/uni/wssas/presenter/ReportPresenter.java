package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.AnnouncementDao;
import com.abc.uni.wssas.dao.DiscussionForumDao;
import com.abc.uni.wssas.dao.ResourceArticlesDao;
import com.abc.uni.wssas.model.Announcement;
import com.abc.uni.wssas.model.DiscussionForum;
import com.abc.uni.wssas.model.Report;
import com.abc.uni.wssas.model.ResourceArticles;

@RestController
@RequestMapping("/report")
@CrossOrigin
public class ReportPresenter {

	@Autowired
	DiscussionForumDao discussionForumDao;
	
	@Autowired
	AnnouncementDao announcementDao;
	
	@Autowired
	ResourceArticlesDao resourceArticlesDao;
	
	@PostMapping("/discussion-forum")
	public ResponseEntity<List<DiscussionForum>> getDiscussionForum(@RequestBody Report report){
		List<DiscussionForum> discussionForums= discussionForumDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate());
		return new ResponseEntity<List<DiscussionForum>>(discussionForums, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/announcement")
	public ResponseEntity<List<Announcement>> getAnnouncement(@RequestBody Report report){
		List<Announcement> announcements= announcementDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate());
		return new ResponseEntity<List<Announcement>>(announcements, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/resource-articles")
	public ResponseEntity<List<ResourceArticles>> getResourceArticles(@RequestBody Report report){
		List<ResourceArticles> resourcesArticles= resourceArticlesDao.findByCreatedDateBetween(report.getStartDate(), report.getEndDate());
		return new ResponseEntity<List<ResourceArticles>>(resourcesArticles, new HttpHeaders(), HttpStatus.OK);
	}
}
