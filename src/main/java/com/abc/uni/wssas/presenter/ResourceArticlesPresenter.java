package com.abc.uni.wssas.presenter;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.abc.uni.wssas.dao.ResourceArticlesDao;
import com.abc.uni.wssas.dao.UserDao;
import com.abc.uni.wssas.model.ResourceArticles;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping("/resourceArticle")
@CrossOrigin
public class ResourceArticlesPresenter {
	@Value("${filePath}")
	private String filePath;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	ResourceArticlesDao resourceArticlesDao;
	@Autowired
	UserDao userDao;
	@Autowired
	private ApplicationContext applicationContext;
	@GetMapping("/data")
	public ResponseEntity<List<ResourceArticles>> findAll(){
		List<ResourceArticles> resourceArticles= resourceArticlesDao.findAll();
		return new ResponseEntity<List<ResourceArticles>>(resourceArticles, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/news")
	public ResponseEntity<List<ResourceArticles>> findNews(){
		List<ResourceArticles> resourceArticles= resourceArticlesDao.findByResourceArticles("News");
		return new ResponseEntity<List<ResourceArticles>>(resourceArticles, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/academic")
	public ResponseEntity<List<ResourceArticles>> findAcademicArticles(){
		List<ResourceArticles> resourceArticles= resourceArticlesDao.findByResourceArticles("Academic");
		return new ResponseEntity<List<ResourceArticles>>(resourceArticles, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/non-academic")
	public ResponseEntity<List<ResourceArticles>> findNonAcademicArticles(){
			List<ResourceArticles> resourceArticles= resourceArticlesDao.findByResourceArticles("Non-academic");
		return new ResponseEntity<List<ResourceArticles>>(resourceArticles, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/count")
	public int totalCount() {
		return resourceArticlesDao.findAll().size();
	}
	
	@PostMapping(value = "/save", consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
	public ResponseEntity<ResourceArticles> save(@RequestPart("model") String model, @RequestPart("file") MultipartFile[] files) {
		//String absolutePath = servletContext.getRealPath("resources/uploads");
		String fileNames = new String();
		for (int i = 0; i < files.length; i++) {
			File fileToSave = new File(filePath + files[i].getOriginalFilename());
			//copy file content from received file to new local file
			try {
				files[i].transferTo(fileToSave);
				fileNames+=files[i].getOriginalFilename()+",";
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(String.format("File name '%s' uploaded successfully.", files[i].getOriginalFilename()));
		}
		ObjectMapper mapper = new ObjectMapper();
		ResourceArticles obj=new ResourceArticles();
		//JSON file to Java object
		try {
			obj = mapper.readValue(model, ResourceArticles.class);
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		if (fileNames != null && fileNames.length() > 0 && fileNames.charAt(fileNames.length() - 1) == ',') {
			fileNames = fileNames.substring(0, fileNames.length() - 1);
			obj.setFileNames(fileNames);

		}
		ResourceArticles resourceArticlesEntity= resourceArticlesDao.save(obj);
		return new ResponseEntity<ResourceArticles>(resourceArticlesEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/download/{filename:.+}")
	@ResponseBody
	public ResponseEntity serveFile(@PathVariable String filename) {
		
		Resource file = applicationContext.getResource("file:"+filePath + filename);
		if (file.exists()) {
			HttpHeaders headers=new HttpHeaders();
			//instructing web browser how to treat downloaded file
			headers.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + file.getFilename() + "\"");
			//allowing web browser to read additional headers from response
			headers.add("Access-Control-Expose-Headers",HttpHeaders.CONTENT_DISPOSITION + "," + HttpHeaders.CONTENT_LENGTH);
			//put headers and file within response body
			return ResponseEntity.ok().headers(headers).body(file);
		}
		//in case requested file does not exists
		return ResponseEntity.notFound().build();
	}
	@PutMapping("/update/{id}")
	public ResponseEntity<ResourceArticles> update(@PathVariable(value ="id") Long id, @RequestBody ResourceArticles resourceArticles) {
		ResourceArticles dbResourceArticles= resourceArticlesDao.findById(id);
		if(dbResourceArticles == null) {
			return ResponseEntity.notFound().build();
		}
		ResourceArticles resourceArticlesEntity= resourceArticlesDao.update(resourceArticles, id);
		return new ResponseEntity<ResourceArticles>(resourceArticlesEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResourceArticles> findById(@PathVariable(value ="id") Long id) {
		ResourceArticles resourceArticles= resourceArticlesDao.findById(id);
		if(resourceArticles == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			return ResponseEntity.ok().body(resourceArticles);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<ResourceArticles> delete(@PathVariable("id") Long id) {
		ResourceArticles resourceArticles= resourceArticlesDao.findById(id);
		if(resourceArticles == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			resourceArticlesDao.delete(resourceArticles);
			return ResponseEntity.ok().build();
		}
	}
}
