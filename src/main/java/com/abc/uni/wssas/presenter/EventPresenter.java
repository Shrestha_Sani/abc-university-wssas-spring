package com.abc.uni.wssas.presenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.uni.wssas.dao.EventDao;
import com.abc.uni.wssas.model.Event;

@RestController
@RequestMapping("/event")
@CrossOrigin(origins = "http://localhost:3000")
public class EventPresenter {
	
	@Autowired
	EventDao eventDao;
	
	@GetMapping("/data")
	public ResponseEntity<List<Event>> findAll(){
		List<Event> events= eventDao.findAll();
		return new ResponseEntity<List<Event>>(events, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<Event> save(@RequestBody Event event) {
		Event eventEntity= eventDao.save(event);
		return new ResponseEntity<Event>(eventEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Event> update(@PathVariable(value ="id") Long id, @RequestBody Event event) {
		Event dbEvent= eventDao.findById(id);
		if(dbEvent == null) {
			return ResponseEntity.notFound().build();
		}
		Event eventEntity= eventDao.update(event, id);
		return new ResponseEntity<Event>(eventEntity, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Event> delete(@PathVariable("id") Long id) {
		Event event= eventDao.findById(id);
		if(event == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			eventDao.delete(event);
			return ResponseEntity.ok().build();
		}
	}
}
