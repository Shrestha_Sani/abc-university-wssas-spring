package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Role;
import com.abc.uni.wssas.respository.RoleRepository;

@Service
public class RoleDao {
	@Autowired
	RoleRepository repository;
	
	public Role save(Role role) {
		return repository.save(role);
	}
	
	public List<Role> getRoles(){
		return repository.findTop3ByOrderByIdAsc();
	}
	
	public List<Role> findAll(){
		List<Role> roles= repository.findAll();
		System.out.println("role size is "+roles.size());
		for(Role role: roles) {
			System.out.println(role);

		}
		return repository.findAll();
	}
	
	public Role findById(Long id) {
		Optional<Role> role= repository.findById(id);
		if(role.isPresent()) {
			return role.get();
		}
		else
			return null;
	}
	
	public void delete(Role role) {
		repository.delete(role);
	}
	
}
