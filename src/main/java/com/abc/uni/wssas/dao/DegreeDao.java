package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Degree;
import com.abc.uni.wssas.respository.DegreeRepository;

@Service
public class DegreeDao {
	@Autowired
	DegreeRepository repository;
	
	public Degree save(Degree degree) {
		return repository.save(degree);
	}
	
	public List<Degree> findAll(){
		return repository.findAll();
	}
	
	public Degree findById(Long id) {
		Optional<Degree> degree= repository.findById(id);
		if(degree.isPresent()) {
			return degree.get();
		}
		else
			return null;
	}
	
	public void delete(Degree degree) {
		repository.delete(degree);
	}

	public Degree update(Degree degree, Long id) {
		Degree dbDegree= findById(id);
		dbDegree.setDegreeName(degree.getDegreeName());
		dbDegree.setDuration(degree.getDuration());
		return repository.save(dbDegree);
	}
}
