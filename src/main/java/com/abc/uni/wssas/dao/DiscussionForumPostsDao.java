package com.abc.uni.wssas.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.DiscussionForumPosts;
import com.abc.uni.wssas.respository.DiscussionForumPostsRepository;

@Service
public class DiscussionForumPostsDao {
	
	@Autowired
	DiscussionForumPostsRepository repository;
	
	@Autowired
	UserDao userDao;

	@Autowired
	DiscussionForumDao discussionForumDao;
	
	public List<DiscussionForumPosts> findByDiscussionForum(Long id){
		return repository.findByDiscussionForumOrderByPostedDateAsc(discussionForumDao.findById(id));
	}
	
	public DiscussionForumPosts save(DiscussionForumPosts discussionForumPosts) {
		discussionForumPosts.setUser(userDao.findByUserName(discussionForumPosts.getUser().getUsername()));
		discussionForumPosts.setDiscussionForum(discussionForumDao.findById(discussionForumPosts.getDiscussionForum().getId()));
		discussionForumPosts.setPostedDate(new Date());
		
		return repository.save(discussionForumPosts);
	}
	
	public List<DiscussionForumPosts> findAll(){
		return repository.findAll();
	}
	
	public DiscussionForumPosts findById(Long id) {
		Optional<DiscussionForumPosts> discussionForumPosts= repository.findById(id);
		if(discussionForumPosts.isPresent()) {
			return discussionForumPosts.get();
		}
		else
			return null;
	}
	
	public DiscussionForumPosts update(DiscussionForumPosts discussionForumPost, Long id) {
		DiscussionForumPosts dbDiscussionForumPost = findById(id);
		
		return repository.save(dbDiscussionForumPost);
	}
	
	public void delete(DiscussionForumPosts discussionForumPosts) {
		repository.delete(discussionForumPosts);
	}
	

}
