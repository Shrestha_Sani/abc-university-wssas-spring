package com.abc.uni.wssas.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.ResourceArticles;
import com.abc.uni.wssas.respository.ResourceArticlesRepository;

@Service
public class ResourceArticlesDao {
	
	@Autowired
	ResourceArticlesRepository repository;
	
	@Autowired
	UserDao userDao;
	
	public ResourceArticles save(ResourceArticles resourceArticles) {
		resourceArticles.setUser(userDao.findByUserName(resourceArticles.getUser().getUsername()));
		resourceArticles.setCreatedDate(new Date());
		return repository.save(resourceArticles);
	}
	
	public List<ResourceArticles> findAll(){
		return repository.findAll();
	}
	
	public List<ResourceArticles> findByResourceArticles(String resourceType){
		return repository.findByResourceType(resourceType);
	}
	
	public List<ResourceArticles> findByCreatedDateBetween(Date fromDate, Date toDate){
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<ResourceArticles> getYesterdayData() {
		Date yesterday = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(yesterday, yesterday);
	}
	
	public List<ResourceArticles> getLastWeekData() {
		Date fromDate = new Date(System.currentTimeMillis() - 10080 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<ResourceArticles> getLast15daysData() {	
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<ResourceArticles> getLastMonthData() {
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000 - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public ResourceArticles findById(Long id) {
		Optional<ResourceArticles> resourceArticles= repository.findById(id);
		if(resourceArticles.isPresent()) {
			return resourceArticles.get();
		}
		else
			return null;
	}
	
	public ResourceArticles update(ResourceArticles resourceArticles, Long id) {
		ResourceArticles dbResourceArticles = findById(id);
		dbResourceArticles.setUpdatedBy(userDao.findByUserName(resourceArticles.getUpdatedBy().getUsername()));
		dbResourceArticles.setUpdatedDate(new Date());
		dbResourceArticles.setTitle(resourceArticles.getTitle());
		dbResourceArticles.setResourceType(resourceArticles.getResourceType());
		dbResourceArticles.setDescription(resourceArticles.getDescription());

		return repository.save(dbResourceArticles);
	}
	
	public void delete(ResourceArticles resourceArticles) {
		repository.delete(resourceArticles);
	}
	

}
