package com.abc.uni.wssas.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.DiscussionForum;
import com.abc.uni.wssas.respository.DiscussionForumRepository;

@Service
public class DiscussionForumDao {
	
	@Autowired
	DiscussionForumRepository repository;
	
	@Autowired
	CourseDao courseDao;
	
	@Autowired
	UserDao userDao;
	
	public DiscussionForum save(DiscussionForum discussionForum) {
		discussionForum.setCourse(courseDao.findById(discussionForum.getCourse().getId()));
		discussionForum.setCreatedDate(new Date());
		discussionForum.setUser(userDao.findByUserName(discussionForum.getUser().getUsername()));
		return repository.save(discussionForum);
	}
	
	public List<DiscussionForum> findAll(){
		List<DiscussionForum> discussionForumList= repository.findAll();
		discussionForumList.forEach(d -> d.setTotalPosts(d.getDiscussionForumPosts().size()));
		return discussionForumList;
	}
	
	public List<DiscussionForum> findByCreatedDateBetween(Date fromDate, Date toDate){
		List<DiscussionForum> discussionForumList= repository.findByCreatedDateBetween(fromDate, toDate);
		discussionForumList.forEach(d -> d.setTotalPosts(d.getDiscussionForumPosts().size()));
		return discussionForumList;
	}
	
	public List<DiscussionForum> getYesterdayData() {
		Date yesterday = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(yesterday, yesterday);
	}
	
	public List<DiscussionForum> getLastWeekData() {
		Date fromDate = new Date(System.currentTimeMillis() - 10080 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<DiscussionForum> getLast15daysData() {
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<DiscussionForum> getLastMonthData() {
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000 - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public DiscussionForum findById(Long id) {
		Optional<DiscussionForum> discussionForum = repository.findById(id);
		if(discussionForum.isPresent()) {
			return discussionForum.get();
		}
		else
			return null;
	}
	
	public int countDiscussionForumPosts(Long id) {
		Optional<DiscussionForum> discussionForum = repository.findById(id);
		int posts=0;
		if(discussionForum.isPresent()) {
			posts= discussionForum.get().getDiscussionForumPosts().size();
		}
		return posts;
	}
	
	public DiscussionForum update(DiscussionForum discussionForum, Long id) {
		DiscussionForum dbDiscussionForum = findById(id);
		dbDiscussionForum.setUpdatedBy(userDao.findByUserName(discussionForum.getUpdatedBy().getUsername()));
		dbDiscussionForum.setUpdatedDate(new Date());
		dbDiscussionForum.setSubject(discussionForum.getSubject());
		dbDiscussionForum.setMessage(discussionForum.getMessage());
		dbDiscussionForum.setCourse(courseDao.findById(discussionForum.getCourse().getId()));
		return repository.save(dbDiscussionForum);
	}

	public void delete(DiscussionForum discussionForum) {
		repository.delete(discussionForum);
	}
	

}
