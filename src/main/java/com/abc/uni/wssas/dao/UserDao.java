package com.abc.uni.wssas.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.ChangePasswordRequest;
import com.abc.uni.wssas.model.ChangePasswordResponse;
import com.abc.uni.wssas.model.Course;
import com.abc.uni.wssas.model.User;
import com.abc.uni.wssas.respository.UserRepository;

@Service
public class UserDao {

	@Autowired
	UserRepository repository;

	@Autowired
	RoleDao roleDao;

	@Autowired
	AddressDao addressDao;

	@Autowired
	CourseDao courseDao;

	@Autowired
	private AuthenticationManager authenticationManager;

	public User save(User user) {
		User dbUser=findByUserName(user.getUsername());
		if(dbUser !=null) {
			user.setDuplicateUser(true);
			return user;
		}
		user.setPassword(getBCryptedPassword(user.getPassword()));
		user.setRole(roleDao.findById(Long.valueOf(user.getRole().getId())));
		Long courseId = user.getCourses().get(0).getId();
		List<Course> courseList = new ArrayList<>();
		courseList.add(courseDao.findById(courseId));
		user.setCourses(courseList);
		user.setDuplicateUser(false);

		return repository.save(user);
	}

	public User update(User user, Long id) {
		User dbUser = findById(id);
		dbUser.setAddress(addressDao.save(user.getAddress()));
		dbUser.setRole(roleDao.findById(Long.valueOf(user.getRole().getId())));
		dbUser.setFirstName(user.getFirstName());
		dbUser.setLastName(user.getLastName());
		dbUser.setMiddleName(user.getMiddleName());
		dbUser.setDob(user.getDob());
		dbUser.setPhoneNum(user.getPhoneNum());
		dbUser.setGender(user.getGender());
		dbUser.setPassword(getBCryptedPassword(user.getPassword()));

		return repository.save(dbUser);
	}

	public List<User> findAll() {
		return repository.findAll();
	}

	public int getStudentCount() {
		Predicate<User> studentUser = user -> user.getRole().getRoleName().equals("STUDENT");
		List<User> users = findAll().stream().filter(studentUser).collect(Collectors.toList());
		return users.size();

	}

	public int getStaffCount() {
		Predicate<User> staffUser = user -> user.getRole().getRoleName().equals("STAFF");
		List<User> users = findAll().stream().filter(staffUser).collect(Collectors.toList());
		return users.size();

	}

	public User findById(Long id) {
		Optional<User> user = repository.findById(id);
		if (user.isPresent()) {
			return user.get();
		} else
			return null;
	}

	public void delete(User user) {
		repository.delete(user);
	}

	public User findByUserName(String username) {
		return repository.findByUsername(username);
	}

	public String getBCryptedPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	public ChangePasswordResponse changePassword(ChangePasswordRequest request) {
		ChangePasswordResponse response = new ChangePasswordResponse();
		if (request.getNewPassword().equals(request.getConformPassword())) {
			try {
				authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(request.getUsername(), request.getCurrentPassword()));
				User dbUser = findByUserName(request.getUsername());
				dbUser.setPassword(getBCryptedPassword(request.getNewPassword()));
				repository.save(dbUser);
				response.setChanged(true);
				response.setMatched(true);
				response.setMessage("Your password is changed");
			} catch (Exception exception) {
				response.setChanged(false);
				response.setMatched(true);
				response.setMessage("Your password is incorrect");
			}
		} else {
			response.setMatched(false);
			response.setChanged(false);
			response.setMessage("Password donot match");
		}
		return response;
	}
}
