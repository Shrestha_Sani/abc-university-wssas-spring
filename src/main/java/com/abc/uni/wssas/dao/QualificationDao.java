package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Qualification;
import com.abc.uni.wssas.respository.QualificationRepository;

@Service
public class QualificationDao {
	
	@Autowired
	QualificationRepository repository;
	
	public Qualification save(Qualification qualification) {
		return repository.save(qualification);
	}
	
	public List<Qualification> findAll(){
		return repository.findAll();
	}
	
	public Qualification findById(Long id) {
		Optional<Qualification> qualification= repository.findById(id);
		if(qualification.isPresent()) {
			return qualification.get();
		}
		else
			return null;
	}
	
	public void delete(Qualification qualification) {
		repository.delete(qualification);
	}
	

}
