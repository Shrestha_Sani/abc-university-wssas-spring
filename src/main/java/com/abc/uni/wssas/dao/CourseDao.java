package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Course;
import com.abc.uni.wssas.respository.CourseRepository;


@Service
public class CourseDao {
	
	@Autowired
	CourseRepository repository;
	
	public Course save(Course course) {
		return repository.save(course);
	}
	
	public List<Course> findAll(){
		return repository.findAll();
	}
	
	public Course findById(Long id) {
		Optional<Course> course= repository.findById(id);
		if(course.isPresent()) {
			return course.get();
		}
		else
			return null;
	}
	
	public Course update(Course course, Long id) {
		Course dbCourse= findById(id);
		dbCourse.setCourseCode(course.getCourseCode());
		dbCourse.setCourseName(course.getCourseName());
		return repository.save(dbCourse);
	}
	
	
	public void delete(Course course) {
		repository.delete(course);
	}
	
}
