package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Event;
import com.abc.uni.wssas.respository.EventRepository;

@Service
public class EventDao {
	
	@Autowired
	EventRepository repository;
	
	public Event save(Event event) {
		return repository.save(event);
	}
	
	public List<Event> findAll(){
		return repository.findAll();
	}
	
	public Event findById(Long id) {
		Optional<Event> event= repository.findById(id);
		if(event.isPresent()) {
			return event.get();
		}
		else
			return null;
	}
	
	public Event update(Event event, Long id) {
		Event dbEvent= findById(id);
		dbEvent.setEventType(event.getEventType());
		return repository.save(dbEvent);
	}
	
	public void delete(Event event) {
		repository.delete(event);
	}
	

}
