package com.abc.uni.wssas.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Announcement;
import com.abc.uni.wssas.respository.AnnouncementRepository;

@Service
public class AnnouncementDao {

	@Autowired
	AnnouncementRepository repository;

	@Autowired
	EventDao eventDao;
	
	@Autowired
	UserDao userDao;

	public Announcement save(Announcement announcement) {
		announcement.setCreatedDate(new Date());
		announcement.setUser(userDao.findByUserName(announcement.getUser().getUsername()));
		announcement.setEvent(eventDao.findById(announcement.getEvent().getId()));
		return repository.save(announcement);
	}

	public List<Announcement> findAll() {
		return repository.findAll();
	}

	public List<Announcement> findByCreatedDateBetween(Date fromDate, Date toDate){
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<Announcement> getYesterdayData() {
		Date yesterday = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(yesterday, yesterday);
	}
	
	public List<Announcement> getLastWeekData() {
		Date fromDate = new Date(System.currentTimeMillis() - 10080 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<Announcement> getLast15daysData() {
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public List<Announcement> getLastMonthData() {
		Date fromDate = new Date(System.currentTimeMillis() - 21600 * 60000 - 21600 * 60000);
		Date toDate = new Date(System.currentTimeMillis() - 1440 * 60000);
		return repository.findByCreatedDateBetween(fromDate, toDate);
	}
	
	public int getNewAnnouncementCount() {
		Predicate<Announcement> notifiedAnnouncement = announcement -> announcement.isNotified() == true;
		List<Announcement> announcements = findAll().stream().filter(notifiedAnnouncement).collect(Collectors.toList());
		return announcements.size();
	}

	public List<Announcement> getNotifications() {
		Date date = new Date(System.currentTimeMillis() - 10080 * 60000);
		return repository.findByIsNotifiedAndNotifiedDateGreaterThanEqual(true, date);
	}

	public Announcement findById(Long id) {
		Optional<Announcement> announcement = repository.findById(id);
		if (announcement.isPresent()) {
			return announcement.get();
		} else
			return null;
	}

	public void delete(Announcement announcement) {
		repository.delete(announcement);
	}

	public Announcement update(Announcement announcement, Long id) {
		Announcement dbAnnouncement = findById(id);
		dbAnnouncement.setUpdatedBy(userDao.findByUserName(announcement.getUpdatedBy().getUsername()));
		dbAnnouncement.setUpdatedDate(new Date());
		dbAnnouncement.setEventDate(announcement.getEventDate());
		dbAnnouncement.setEventDescription(announcement.getEventDescription());
		dbAnnouncement.setEventName(announcement.getEventName());
		dbAnnouncement.setEvent(eventDao.findById(announcement.getEvent().getId()));

		return repository.save(dbAnnouncement);
	}

	public Announcement notify(Long id) {
		Announcement dbAnnouncement = findById(id);
		dbAnnouncement.setNotified(true);
		dbAnnouncement.setNotifiedDate(new Date());
		return repository.save(dbAnnouncement);
	}
	
}
