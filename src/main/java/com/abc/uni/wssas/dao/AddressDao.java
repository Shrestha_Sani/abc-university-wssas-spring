package com.abc.uni.wssas.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.Address;
import com.abc.uni.wssas.respository.AddressRepository;


@Service
public class AddressDao {
	@Autowired
	AddressRepository repository;
	
	public Address save(Address address) {
		Address addressS= repository.save(address);
		return addressS;
	}
	
	public List<Address> findAll(){
		return repository.findAll();
	}
	
	public Address findById(Long id) {
		Optional<Address> address= repository.findById(id);
		if(address.isPresent()) {
			return address.get();
		}
		else
			return null;
	}
	
	public void delete(Address address) {
		repository.delete(address);
	}
	
}
