package com.abc.uni.wssas;

import java.io.Serializable;

public class LoginSuccess implements Serializable {
	private static final long serialVersionUID = -8091879091924046844L;

	private final String jwtToken;
	private final String username;
	private final String role;
	private  final boolean authenticated;
	private  final String message;

	public LoginSuccess(String jwtToken, String username, String role, Boolean authenticated, String message) {
		this.jwtToken = jwtToken;
		this.username = username;
		this.role = role;
		this.authenticated = authenticated;
		this.message = message;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public String getMessage() {
		return message;
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public String getUsername() {
		return username;
	}

	public String getRole() {
		return role;
	}
	
	
}
