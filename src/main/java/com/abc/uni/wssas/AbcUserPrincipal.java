package com.abc.uni.wssas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.abc.uni.wssas.model.User;


public class AbcUserPrincipal implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private User user;

	public AbcUserPrincipal(User user) {
		super();
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities= new ArrayList<>();
		authorities.add( new SimpleGrantedAuthority("ROLE_"+ user.getRole().getRoleName().toUpperCase()));
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
//		 TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
//		 TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
//		 TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
//		 TODO Auto-generated method stub
		return true;
	}

	
	
}

