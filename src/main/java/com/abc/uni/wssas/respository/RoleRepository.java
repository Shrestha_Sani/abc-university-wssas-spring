package com.abc.uni.wssas.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	
List<Role> findTop3ByOrderByIdAsc();
}
