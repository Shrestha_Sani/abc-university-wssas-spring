package com.abc.uni.wssas.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.DiscussionForum;
import com.abc.uni.wssas.model.DiscussionForumPosts;

@Repository
public interface DiscussionForumPostsRepository extends JpaRepository<DiscussionForumPosts, Long> {
	List<DiscussionForumPosts> findByDiscussionForumOrderByPostedDateAsc(DiscussionForum discussionForum);
}
