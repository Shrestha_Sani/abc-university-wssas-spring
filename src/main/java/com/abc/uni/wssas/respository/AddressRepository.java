package com.abc.uni.wssas.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
