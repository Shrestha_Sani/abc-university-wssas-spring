package com.abc.uni.wssas.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abc.uni.wssas.model.Qualification;

public interface QualificationRepository extends JpaRepository<Qualification, Long> {

}
