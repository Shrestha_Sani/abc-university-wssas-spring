package com.abc.uni.wssas.respository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.Announcement;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long>{
	List<Announcement> findByIsNotifiedAndNotifiedDateGreaterThanEqual(Boolean isNotified, Date date);
	List<Announcement> findByCreatedDateBetween(Date fromDate, Date toDate);
	}

	



