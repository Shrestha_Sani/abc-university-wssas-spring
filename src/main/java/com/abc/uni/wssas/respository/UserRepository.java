package com.abc.uni.wssas.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String userId);
	
}
