package com.abc.uni.wssas.respository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abc.uni.wssas.model.Announcement;
import com.abc.uni.wssas.model.DiscussionForum;

@Repository
public interface DiscussionForumRepository extends JpaRepository<DiscussionForum, Long>{
	List<DiscussionForum> findByCreatedDateBetween(Date fromDate, Date toDate);

}
