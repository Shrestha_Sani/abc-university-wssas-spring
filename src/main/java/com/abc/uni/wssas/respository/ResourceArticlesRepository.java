package com.abc.uni.wssas.respository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abc.uni.wssas.model.ResourceArticles;

public interface ResourceArticlesRepository extends JpaRepository<ResourceArticles, Long> {
	List<ResourceArticles> findByCreatedDateBetween(Date fromDate, Date toDate);
	List<ResourceArticles> findByResourceType(String resourceType);
}
