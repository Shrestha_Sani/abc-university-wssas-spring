package com.abc.uni.wssas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.abc.uni.wssas.model.User;
import com.abc.uni.wssas.respository.UserRepository;

@Service
public class AbcUserDetailsService implements UserDetailsService {
	@Autowired
	UserRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user= repo.findByUsername(username);
		if(user==null)
			throw new UsernameNotFoundException("Please enter valid username");
		
		return new AbcUserPrincipal(user);
	}

}

