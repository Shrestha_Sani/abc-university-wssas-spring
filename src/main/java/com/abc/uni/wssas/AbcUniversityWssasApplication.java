package com.abc.uni.wssas;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.abc.uni.wssas.model.Role;
import com.abc.uni.wssas.respository.RoleRepository;

@SpringBootApplication
public class AbcUniversityWssasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbcUniversityWssasApplication.class, args);
	}

	@Bean
    CommandLineRunner init (RoleRepository roleRepo){
		
        return args -> {
            Role roleAdmin= new Role();
            roleAdmin.setRoleName("ADMIN");
            roleAdmin.setDescription("This is Admin Role");
            roleRepo.save(roleAdmin);
            
            Role roleStaff = new Role();
            roleStaff.setRoleName("STAFF");
            roleStaff.setDescription("This is Staff Role");
            roleRepo.save(roleStaff);
            
            Role roleStudent = new Role();
            roleStudent.setRoleName("STUDENT");
            roleStudent.setDescription("This is Student Role");
            roleRepo.save(roleStudent);
        };
    }

}
